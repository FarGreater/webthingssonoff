/**
 * ****************************************************************************
 * 
 * * WebThingsSonoff
 * 
 * ****************************************************************************
 * By Michael Egtved Christensen 2019 ->
 * 
 * see https://github.com/esp8266/Arduino for setting up the Arduino enviroment
 * 
 * Generic esp8266 module
 * Gl Sonof DIO
 * Ny sonof DOUT
 *     Flash Frequency: 40MHz
 *     Upload Using: Serial
 *     CPU Frequency: 80MHz
 *     Flash Size: 1M (64K SPIFFS)
 *     Debug Port: Disabled
 *     Debug Level: None
 *     Reset Method: ck
 *     Upload Speed: 115200 * 
 * 
 *  Programmer AVRISP mkII
 *  tyk på  knappen på sono før der brændes 
 *  see https://github.com/arendst/Sonoff-MQTT-OTA-Arduino fir Sonoff board config 
 * 
 * LEDs on SONOFF
 * Basic
 *  Green led        GPIO13
 * Relay            GPIO12
 * S20
 * Green led        GPIO13
 * Relay/blue led   GPIO12
 * 
 * * Fremhæv
 * ! Advarsel
 * ? Spørgsmål
 * TODO: TODO
 * @param parameter
*/

#include <MecLogging.h>
#include <MecWifi.h>
#include <MecWebThing.h>

/**
 * ****************************************************************************
 *  GLOBALS
 * ****************************************************************************
 */ 
#define S20 1
#define Basic 2
#define model S20

// Setup for Sonoff Basic
//int greenLedPin = D7; // Green LED
//int redLedPin = D5;   // Red LED
//int relayPin = D6;    // Relay connected to digital pin 12 (on s20 together with blue led)
//int btnPin = D3;      // Push button

// Setup for Sonoff S20
int greenLedPin = D7; // Green LED
int redLedPin = D5;   // does not exist
int relayPin = D6;    // Relay connected to digital pin 12 (on s20 together with blue led)
int btnPin = D3;      // Push button

int UpdateTimerTimer = 0;
int TimeOffTimerInMillis = 120 * 60 * 1000;
boolean ReleyStatus = false;

int btn = 1;
int btntimer = 0;

/**
 * ****************************************************************************
 *  Setting up Debug
 * ****************************************************************************
 */
// LED to flash when there is activity
#define ActivityLedMode None // None, Inverse or Normal
// Debug level
//    - Inf for "production"
//    - Deb for "debug/development"
//    see libary Debug.h for all the debug levels
#define MyLogLevel Deb

MecLogging MyLog(MyLogLevel, greenLedPin, ActivityLedMode);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */
String WiFiSSID[] = {"Clemens 5n",
                     "Clemens 5nn",
                     "Michaels Phone",
                     "Clemens-5G"};
String WiFiPassword[] = {"zurtkeld",
                         "zurtkeld",
                         "zurtkeld",
                         "zurtkeld"};
int WifiCount = 4;

String ThingID = "SonoffKontakt_III";

MecWifi MyWifi(WiFiSSID, WiFiPassword, WifiCount, &MyLog, ThingID);

/**
 * ****************************************************************************
 *  Setting up Wifi
 * ****************************************************************************
 */

String ThingType[] = {"Light",
                      "SmartPlug",
                      "OnOffSwitch"};
int ThingTypeCount = 3;

MecThing Thing(ThingID, "Sonoff Kontakt III", "WebThing Sonoff Kontakt III", ThingType, ThingTypeCount, &MyLog, &MyWifi);

MecProperty PropOnOff(&MyLog, "OnOffProperty", "Tænd/Sluk kontakt", "Tænd sluk for rælætet", true, "OnOffProperty");
MecProperty PropTimerEnable(&MyLog, "TimerEnable", "Tænd/Sluk for timeren", "Tænd sluk for timeren", true);
MecProperty PropTimer(&MyLog, "timer", "Minutter til sluk", "Timer, i minutter til sluk", 120);

/**
 * ****************************************************************************
 *  Reley On/Off
 * ****************************************************************************
 */

void Reley(bool state)
{
  MyLog.infoNl("Reley: New Status: " + String(state), Deb);
  PropOnOff.SetValue(state);

  ReleyStatus = state;

  if (model == Basic)
  {
    digitalWrite(greenLedPin, !state); // sets the LED on
    digitalWrite(relayPin, state);     // sets the LED on
  }
  else
  {
    digitalWrite(relayPin, state); // sets the LED on
  }
}

/**
 * ****************************************************************************
 * ****************************************************************************
 *  setup
 * ****************************************************************************
 */
void setup()
{
  // Init libaries
  MyLog.ActivityLED(LedON);
  MyLog.setup();
  MyWifi.setup();

  Thing.addProperty(&PropOnOff);
  Thing.addProperty(&PropTimerEnable);
  Thing.addProperty(&PropTimer);

  if (model == Basic)
  {
    pinMode(greenLedPin, OUTPUT); // sets the digital pin as output
    pinMode(redLedPin, OUTPUT);   // sets the digital pin as output
    pinMode(relayPin, OUTPUT);    // sets the digital pin as output
    pinMode(btnPin, INPUT);       // sets the digital pin as Input
    //digitalWrite(ledPin, HIGH);
  }
  else
  {
    pinMode(greenLedPin, OUTPUT); // sets the digital pin as output
    pinMode(relayPin, OUTPUT);    // sets the digital pin as output
    pinMode(btnPin, INPUT);       // sets the digital pin as Input
  }

  Reley(true); // power on at startup

  Thing.setup();

  MyLog.infoNl("Setup- start...", Deb);

  TimeOffTimerInMillis = 120 * 60 * 1000 + millis();
  Reley(true);

  // publish end setup
  MyLog.infoNl("Setup- End...", Deb);
  MyLog.ActivityLED(LedOFF);
}

/**
 * ****************************************************************************
 * ****************************************************************************
 *  loop
 * ****************************************************************************
 */
void loop()
{
  // Loop libaries
  MyLog.ActivityLED(LedON);
  MyWifi.loop();
  MyLog.loop();
  Thing.loop();

  // put your main code here, to run repeatedly:

  //////////////////
  //
  // Knappen
  //
  //////////////////
  //
  // Er knappen trykket
  int val = digitalRead(btnPin);
  //
  // Hvis knappen bliver trykket gemmer vi tiden for trykket.
  //
  if (val == 0 && btntimer == 0)
  {
    btntimer = millis();
  }
  //
  // for S20
  //
  if (model == Basic)
  {
    if (val == 0)
    {
      digitalWrite(redLedPin, LOW); // sets the LED on
    }
    else
    {
      digitalWrite(redLedPin, HIGH); // sets the LED off
    }
  }
  else
  {
    if (val == 0)
    {
      digitalWrite(greenLedPin, LOW); // sets the LED on
    }
    else
    {
      digitalWrite(greenLedPin, HIGH); // sets the LED off
    }
  }
  //
  // Naar knappen er sluppet, saa ud fra laengden, enten
  // skift mellem taendt eller slukket, eller foroeg timeren med 30 minutter
  //
  //

  if (val == 1 && btn == 0)
  {
    MyLog.infoNl("KNAPP- Knap trykket i " + String(millis() - btntimer) + "millisekunder", Inf);
    if ((millis() - btntimer) > 500)
    {
      MyLog.infoNl("LongPUSH", Inf);
      if (PropOnOff.GetBooleanValue() == true)
      {
        PropOnOff.SetValue(false, _ChangedFromRemote);
      }
      else
      {
        PropOnOff.SetValue(true, _ChangedFromRemote);
      }
    }
    else
    {
      MyLog.infoNl("ShortPUSH", Inf);
      int _timeLeft = PropTimer.GetIntegerValue();
      if (PropOnOff.GetBooleanValue() == true)
      {
        PropOnOff.SetValue(false, _ChangedFromRemote);
      }
      else
      {
        PropTimer.SetValue(120, _ChangedFromRemote);
        PropOnOff.SetValue(true, _ChangedFromRemote);
        PropTimerEnable.SetValue(true, _ChangedFromRemote);
      }
    };

    btntimer = 0;
  };

  btn = val;

  if (PropTimer.IsChanged() == true)
  {
    // Hvis timerværdien ændres så
    // * check om den er under nul
    // * Tænd for timeren
    // * tænd for relæet
    MyLog.infoNl("Loop: PropTimer opdateret", Inf);
    MyLog.ActivityLED(LedActivity);
    if (PropTimer.GetIntegerValue() < 1)
    {
      PropTimer.SetValue((int)0, _ChangedFromRemote);
    }
    else
    {
      PropOnOff.SetValue(true, _ChangedFromRemote);
      PropTimerEnable.SetValue(true, _ChangedFromRemote);
    }
    TimeOffTimerInMillis = PropTimer.GetIntegerValue() * 60000 + millis();
  }

  if (UpdateTimerTimer < millis())
  {
    // opdaterer den timeren (tæller ned), men kun hvis der er tænt for timeren
    if (PropTimerEnable.GetBooleanValue() == true)
    {
      MyLog.infoNl("Loop: UpdateTimerTimer loop", Inf);
      MyLog.ActivityLED(LedActivity);
      //TimeOffTimerInMillis = TimeOffTimerInMillis - 10000;

      if (TimeOffTimerInMillis > millis())
      {
        int minsleft = (TimeOffTimerInMillis - millis()) / 60000;
        if (minsleft < 0)
        {
          minsleft = 0;
        }
        MyLog.infoNl("timeLeft: " + String(minsleft) + " minutter", Inf);
        //Tnumber.SetValue(int(TimeOffTimerInMillis - millis()) / 1000);
        PropTimer.SetValue(minsleft);
      };
    };
    UpdateTimerTimer = millis() + 5000;
  };

  if (PropTimerEnable.IsChanged())
  {
    // der er nogen der har ændret "TimerEnable"
    MyLog.infoNl("Loop: PropTimerEnable Changed", Inf);
    MyLog.ActivityLED(LedActivity);
    if (PropTimerEnable.GetBooleanValue() == true && PropTimer.GetIntegerValue() > 0)
    {
      // hvis timer værdien er stører end 0 så tænder vi relæet og starter timeren i gen.

      TimeOffTimerInMillis = PropTimer.GetIntegerValue() * 60000 + millis();
      Reley(true);
    }
  }

  if (PropOnOff.IsChanged() == true)
  {
    MyLog.infoNl("Loop: Nogen har ændret status på Onoff knappen", Deb);
    MyLog.ActivityLED(LedActivity);
    // der er nogen der har ændret "onoff"
    if (PropOnOff.GetBooleanValue() == true)
    {
      Reley(true);
      if (PropTimerEnable.GetBooleanValue() == true && PropTimer.GetIntegerValue() < 1)
      {
        PropTimer.SetValue((int)120);
      }
    }
    else
    {
      PropTimerEnable.SetValue(false);
      Reley(false);
    }
  }

  if (PropTimerEnable.GetBooleanValue() == true && PropTimer.GetIntegerValue() < 1 && ReleyStatus == true)
  {
    MyLog.infoNl("Loop: Tiden er løbet ud slukker for relæet", Deb);
    MyLog.ActivityLED(LedActivity);
    Reley(false);
  }

  // put your main code here, to run repeatedly:

  // publish end loop
  MyLog.ActivityLED(LedOFF);
}
